# Source Link and Reproducible Builds for Open Source Projects

Development meta-package that provides a simple way to set up Source Link and reproducible builds for open source projects.



# Usage

To configure Source Link and reproducible builds, add the following package reference to the project file:
```xml
<Project Sdk="Microsoft.NET.Sdk">
  <ItemGroup>
      <!-- ... -->
    <PackageReference Include="XploRe.SourceLink.OpenSource" Version="1.0.0" PrivateAssets="All" />
  </ItemGroup>
</Project>
```

Setting `PrivateAssets` to `All` ensures that the package reference is used for building only and will not appear as a
dependency in the .nuspec file of the project.
If a tool is used to add the dependency, the `PrivateAssets` property should be set automatically.

Configure additional Source Link properties as needed (e.g. when a private host is used).



# Effects

Referencing the package will automatically add development dependencies for all available Source Link implementations;
a matching one for the currently used VCS is chosen automatically.

Additionally, several build properties are set, including:
- `DebugType` is set to `portable`. This is recommended for packages that are published on nuget.org and configures the
  generation of portable PDBs for distribution via symbol packages.
  See [documentation on how to create a symbol package][dotnet-create-snupkg].
- `Deterministic` is set to `true`. This ensures that the build is reproducible.
- `ContinuousIntegrationBuild` is set to `true` if the build is triggered by a CI system such that source paths are
  normalised for matching sources in the remote repository.
- `PublishRepositoryUrl` is set to `true`. This automatically adds the repository url to the generated .nuspec file.
- `EmbedUntrackedSources` is set to `true`. Untracked sources are source files that are not part of the repository but
  were generated during the build (e.g. `AssemblyInfo.cs`).

Each property is only set if not already set. To overwrite the value, set the property in the project file.



# See Also

- [Source Link Introduction][dotnet-sourcelink]
- [Source Link on GitHub][sourcelink-github]
- [Deterministic Code Generation Details][compiler-deterministic]



# License

Released under the [Apache License, Version 2.0][apache-license-2.0].

Copyright © xplo.re IT Services, Michael Maier. \
All rights reserved.


[apache-license-2.0]: <http://www.apache.org/licenses/LICENSE-2.0> "Apache License 2.0"
[dotnet-create-snupkg]: <https://docs.microsoft.com/en-us/nuget/create-packages/symbol-packages-snupkg> "Creating Symbol Packages (.snupkg)"
[sourcelink-github]: <https://github.com/dotnet/sourcelink> "Source Link on GitHub"
[compiler-deterministic]: <https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/code-generation#deterministic> "Deterministic Code Generation"
[dotnet-sourcelink]: <https://docs.microsoft.com/en-us/dotnet/standard/library-guidance/sourcelink> "Source Link"
