<?xml version="1.0" encoding="utf-8"?>
<!--
 * xplo.re .NET
 *
 * Copyright (C) 2022, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0.
 * See bundled LICENSE.txt for license information.
 -->
<Project xmlns="http://schemas.microsoft.com/developer/msbuild/2003">

  <PropertyGroup>

    <!-- Create portable symbol packages by default, as is recommended for packages published on NuGet.
         https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/code-generation#debugtype -->
    <DebugType Condition=" '$(DebugType)' == '' ">portable</DebugType>

    <!-- Enable deterministic builds.
         https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/code-generation#deterministic -->
    <Deterministic Condition=" '$(Deterministic)' == '' ">true</Deterministic>

  </PropertyGroup>


  <ItemGroup>

    <!-- Required to fetch source control information for GitLab hosted repositories. -->
    <SourceLinkGitLabHost Include="gitlab.com" />

  </ItemGroup>


  <!-- Set CI build flag within CI/CD systems such that source paths are normalised. This is required for Source Link to
       be able to point to the correct repository location, independent of local file system paths.
       This flag must not be set for local builds as this will break source paths for debugging.
        
       Checks for environment variables set by well-known build environments. Values of environment variables are only
       evaluated if these are set to a guaranteed constant value; detection if possible is done via the name of the
       environment variable. -->
  <PropertyGroup Condition=" '$(ContinuousIntegrationBuild)' == '' ">

    <!-- Generic CI marker used by several build systems, including:
         PLATFORM             ALTNERATIVE             DOCUMENTATION
         AppVeyor             APPVEYOR                https://www.appveyor.com/docs/environment-variables/
         Buddy                BUDDY                   https://buddy.works/docs/pipelines/environment-variables#default-environment-variables
         Circle CI            CIRCLECI                https://circleci.com/docs/2.0/env-vars/#built-in-environment-variables
         CloudBees CodeShip   -                       https://docs.cloudbees.com/docs/cloudbees-codeship/latest/basic-builds-and-configuration/set-environment-variables#_default_environment_variables
         Codemagic            CONTINUOUS_INTEGRATION  https://docs.codemagic.io/variables/environment-variables/
         GitHub Actions       GITHUB_ACTION           https://docs.github.com/en/actions/learn-github-actions/environment-variables#default-environment-variables
         Oracle Wercker       WERCKER_*               https://devcenter.wercker.com/administration/environment-variables/available-env-vars/
         Semaphore CI         SEMAPHORE               https://docs.semaphoreci.com/ci-cd-environment/environment-variables/
         Travis CI            TRAVIS                  https://docs.travis-ci.com/user/environment-variables/#default-environment-variables
     -->
    <ContinuousIntegrationBuild Condition=" '$(CI)' == 'true' ">true</ContinuousIntegrationBuild>

    <!-- Atlassian Bamboo.
         Environment variable names are case-sensitive.
         https://confluence.atlassian.com/bamboo/bamboo-variables-289277087.html#Bamboovariables-Build-specificvariables -->
    <ContinuousIntegrationBuild Condition=" '$(bamboo_BuildKey)' != '' ">true</ContinuousIntegrationBuild>

    <!-- AWS CodeBuild.
         https://docs.aws.amazon.com/codebuild/latest/userguide/build-env-ref-env-vars.html -->
    <ContinuousIntegrationBuild Condition=" '$(CODEBUILD_BUILD_ID)' != '' And '$(AWS_REGION)' != '' ">true</ContinuousIntegrationBuild>

    <!-- Azure Pipelines.
         https://docs.microsoft.com/en-us/azure/devops/pipelines/build/variables?view=azure-devops&tabs=yaml#system-variables -->
    <ContinuousIntegrationBuild Condition=" '$(TF_BUILD)' == 'true' ">true</ContinuousIntegrationBuild>

    <!-- GitLab CI/CD.
         Also sets the CI environment variable for runners version 0.4 and above.
         https://docs.gitlab.com/ee/ci/variables/predefined_variables.html -->
    <ContinuousIntegrationBuild Condition=" '$(GITLAB_CI)' == 'true' ">true</ContinuousIntegrationBuild>

    <!-- GoCD.
         https://docs.gocd.org/current/faq/dev_use_current_revision_in_build.html#standard-gocd-environment-variables -->
    <ContinuousIntegrationBuild Condition=" '$(GO_PIPELINE_LABEL)' != '' ">true</ContinuousIntegrationBuild>

    <!-- Google Cloud Build.
         Only four environment variables are set for every configuration: the three tested ones and PROJECT_NUMBER.
         https://cloud.google.com/build/docs/configuring-builds/substitute-variable-values -->
    <ContinuousIntegrationBuild Condition=" '$(BUILD_ID)' != '' And '$(PROJECT_ID)' != '' And '$(LOCATION)' != '' ">true</ContinuousIntegrationBuild>

    <!-- Jenkins.
         The only specific environment variable JENKINS_URL is not set for every configuration.
         https://www.jenkins.io/doc/book/pipeline/jenkinsfile/#using-environment-variables -->
    <ContinuousIntegrationBuild Condition=" '$(BUILD_ID)' != '' And '$(BUILD_URL)' != '' And '$(NODE_NAME)' != '' ">true</ContinuousIntegrationBuild>

    <!-- JetBrains Space.
         https://www.jetbrains.com/help/space/automation-environment-variables.html#general -->
    <ContinuousIntegrationBuild Condition=" '$(JB_SPACE_API_URL)' != '' ">true</ContinuousIntegrationBuild>

    <!-- TeamCity On-Premises.
         Checking for the version is the officially recommended method to determine whether a build is run within TeamCity.
         https://www.jetbrains.com/help/teamcity/predefined-build-parameters.html#Predefined+Server+Build+Parameters -->
    <ContinuousIntegrationBuild Condition=" '$(TEAMCITY_VERSION)' != '' ">true</ContinuousIntegrationBuild>

  </PropertyGroup>

</Project>
